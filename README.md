# Overview

This repository contains the following:

1. The static website of the Hands-on Portal
2. Code, configuration and deployment files for the third-party applications used in the Hands-on Portal
3. Old deployment files that are currently deprecated

The Hands-on Portal wiki is hosted in the following repository:

https://gitlab.com/giannis/hokudai-hop-wiki.git

# Hands-on Portal website

The Hands-on Portal website comprises of a set of self-contained HTML files under the ```content```
sub-directory. The Hands-on Portal wiki, which is the new version of the Hands-on Portal static
website is hosted in the following repository:

https://gitlab.com/giannis/hokudai-hop-wiki

The Hands-on Portal website and wiki are exposed at the following URLs respectively:

* https://hop.meme.hokudai.ac.jp/classic
* https://hop.meme.hokudai.ac.jp/wiki

# Third-party applications

There are currently two third-party applications in the ```thirdparty``` sub-directory. The
Funatsu group's LVSVL application and the Yoshida group's Supernova classifier application.
These two applications are exposed under the following URLs respectively:

* https://dev.meme.hokudai.ac.jp
* https://dev.meme.hokudai.ac.jp/supernova

## Funatsu

The Funatsu group's LVSVL application is exposed in the Webble World cluster under the following URL:

https://dev.meme.hokudai.ac.jp

The application along with its dependencies is contained in the ```thirdparty/funatsu``` sub-directory.
The application is packaged as a [Docker](https://www.docker.com/) 
[image](https://docs.docker.com/engine/tutorials/dockerimages/). 

### Prerequisites

The only dependency for running the Funatsu group's vlsvl application is Docker.
Detailed instructions on how to install Docker on the target machine can be found
at the following page:

https://docs.docker.com/engine/installation/linux/ubuntulinux/

### Building

To build the Docker image, the following command has to be executed inside the ```thirdparty/funatsu``` 
sub-directory:

```
docker build -t registry.gitlab.com/giannis/hokudai-hop:vlsvl .
```

The image can be built in any system that supports Docker. For example, the following pages contain
instructions for installing Docker on Microsoft Windows systems:

* https://docs.docker.com/engine/installation/windows/
* https://www.docker.com/products/docker-toolbox

After the image is built, for easy deployment it has to be pushed to this repository's Docker
registry. The following commands can be used to login and push the newly built image to the
registry, respectively: 

```
docker login registry.gitlab.com
docker push registry.gitlab.com/giannis/hokudai-hop:vlsvl
```

### Deploying

To deploy the lvsvl server, first the latest image has to be pulled from the target machine.
Note that since the registry is open to the public, it's not necessary to login first. Therefore,
the following command pulls the Docker image from the project's registry: 

```
sudo docker pull registry.gitlab.com/giannis/hokudai-hop:vlsvl
```

The latest version of the image can be ran interactively with the following Docker command:

```
sudo docker run --name lvsvl --rm -it -p 8080:80 registry.gitlab.com/giannis/hokudai-hop:vlsvl
```

The above command will run the lvsvl-server image and wait for its termination. Control-C will 
stop the execution of the lvsvl image and remove it from Docker's execution cache (hence the
```--rm``` command-line parameter). The ```-p 8080:80``` parameter, effectively exposes the
container's port 80 to the whole machine's port 8080. Then, the lvsvl-server will be exposed
to the outside world via the machine's domain/IP and port 8080.

Similarly, the following command runs the lvsvl server as a daemon (hence the ```-d``` parameter):

```
sudo docker run --name lvsvl -d -p 8080:80 registry.gitlab.com/giannis/hokudai-hop:vlsvl
```

The Docker images running as daemons can be listed with the following command:

```
sudo docker ps
```

To stop an image by its given name (```--name``` parameter, e.g., ```lvsvl```), the following command
can be used:

```
sudo docker stop lvsvl
```

Even after the image is stopped, it still stays inside Docker's execution cache so that users can
inspect its log and potentially modified state. Therefore, to completely remove the image from
Docker's cache, the following command can be used:

```
sudo docker rm lvsvl
```

## Yoshida

Yoshida group's supernova classifier application is under the sub-directory ```thirdparty/yoshida```. 
The directory contains the following:

1. The Supernova classifier python application (in zip format) 
2. Instructions for installing and executing the classifier under different modes (in docx and pdf formats)
3. An example web service that can be used to expose the classifier to the Webble World platform (web_server.py)
4. An example Webble that can be used to access the classifier web service (under ```thirdparty/yoshida/webble```)

The web service, when run with its default parameters, is accessible via the following URL:

https://dev.meme.hokudai.ac.jp/supernova

The example Webble is already published at the Webble World platform under the id: ```supernovaclassifier```.

The following URL opens Webble World and loads the example webble:

https://wws.meme.hokudai.ac.jp/#/app?webble=supernovaclassifier

### Prerequisites

The file ```setupENG.docx``` (and ```setupENG.pdf```) located inside the ```thirdparty/yoshida``` sub-directory
contains detailed instructions for installing and setting up the machine to run the python supernova
classification application.

Apart from the instructions included in that file, the example web service, implemented in the file
```thirdparty/yoshida/web_server.py``` uses the [Flask](http://flask.pocoo.org/) framework.

The following command installs Flask, under the previously set-up anaconda environment:

```
conda install -c anaconda flask=0.11.1
```

Note that the web service example also uses the [Flask-CORS](https://flask-cors.readthedocs.io/en/latest/)
plugin. The previous command should install this plugin together with the Flask framework. In case, however,
it is omitted from future Flask distributions, it can be installed with the following command: 

```
conda install -c blaze flask-cors=2.1.2 
```

### Deploying

Since the supernova classifier web service is still just a proof of concept, it's not registered with
a service file under [systemd](https://www.freedesktop.org/wiki/Software/systemd/). Instead it can
be deployed with the following command:

```
nohup python web_server.py&
```

### Flask Resources

Flask:
http://flask.pocoo.org/docs/0.11/
http://flask.pocoo.org/docs/0.11/quickstart/
http://flask.pocoo.org/docs/0.11/api/

Uploading files:
http://flask.pocoo.org/docs/0.11/patterns/fileuploads/#improving-uploads

Designing a RESTful API with Python and Flask:
https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask

For rest APIs JSON resources are "returned" to the client instead of rendered html pages. 
The following function is used:
http://flask.pocoo.org/docs/0.10/api/#flask.json.jsonify

# Old deployment files

The sub-directory ```deploy``` contains a Docker file and some [Kubernetes](http://kubernetes.io/)
deployment files that were used to deploy earlier versions of the Hands-on Portal under a Kubernetes
cluster. These files are not used anymore and are not kept up-to-date.
