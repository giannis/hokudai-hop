//======================================================================================================================
// Controllers for Supernova Classifier for Webble World v3.0 (2013)
// Created By: Jonas Sjobergh, Giannis Georgalis
//======================================================================================================================

wblwrld3App.controller('supernovaImageSelectorCtrl', function($scope, $log, $upload, Slot, Enum) {

    //=== PROPERTIES ====================================================================

    $scope.stylesToSlots = {
        DrawingArea: ['width', 'height']
    };

    $scope.customMenu = [];
    $scope.customInteractionBalls = [];

    $scope.displayText = "Yo!";
    
    $scope.filesToUpload = [];
    $scope.filesData = [];
    $scope.isReadyToGetResults = false;

    //=== EVENT HANDLERS ================================================================

    $scope.onFilesAdded = function(files) {

        $scope.filesToUpload = files;
        $scope.images = [];

        if (files == undefined || files.length == 0)
            return;

        $scope.isReadyToGetResults = true;

	    for (var i = 0; i < files.length; ++i) {

            var f = files[i];
            
            if (!f.type.match('image.*'))
                continue;

            var fr = new FileReader();
            fr.onload = e => $scope.$apply(() =>
                $scope.images.push({ style: '', data: e.target.result }));
	        fr.readAsDataURL(f);
        }
    };

    $scope.getResults = function() {

        $upload.upload({
			url: 'https://dev.meme.hokudai.ac.jp/supernova',
			file: $scope.filesToUpload
			
		}).then(function (resp) {

            var result = resp.data;

    	    for (var i = 0; i < $scope.images.length; ++i) {
    	        $scope.images[i].style = result.images[i] ? 'supernova' : 'notsupernova';
    	    }
            $scope.displayText = result.msg;
		});
		
        $scope.isReadyToGetResults = false;
    };

    //===================================================================================
    // Webble template Initialization
    // If any initiation needs to be done when the webble is created it is here that
    // should be executed. the saved def object is sent as a parameter in case it
    // includes data this webble needs to retrieve.
    // If this function is empty and unused it can safely be deleted.
    // Possible content for this function is as follows:
    // *Add own slots
    // *Set the default slot
    // *Set Custom Child Container
    // *Create Value watchers for slots and other values
    //===================================================================================
    $scope.coreCall_Init = function(theInitWblDef){

    	$scope.addPopupMenuItemDisabled('EditCustomMenuItems');
    	$scope.addPopupMenuItemDisabled('EditCustomInteractionObjects');
    	$scope.addPopupMenuItemDisabled('AddCustomSlots');
    
    	var ios = $scope.theInteractionObjects;
    	for(var i = 0, io; i < ios.length; i++){

    	    io = ios[i];
    	    if(io.scope().getName() == 'Resize'){
        		io.scope().setIsEnabled(false);
    	    }
    	    if(io.scope().getName() == 'Rotate'){
        		io.scope().setIsEnabled(false);
    	    }
        }

    	// $scope.theView.parent().draggable('option', 'cancel', '#fieldsToDrag');
    	// $scope.theView.parent().draggable('option', 'cancel', '#fieldsToDrag2');
    	// $scope.theView.parent().draggable('option', 'cancel', 'fieldsToDrag');
    	// $scope.theView.parent().draggable('option', 'cancel', 'fieldsToDrag2');
    };

    //===================================================================================


    //=== CTRL MAIN CODE ======================================================================

});
