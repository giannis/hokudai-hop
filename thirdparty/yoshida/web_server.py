import os
from flask import Flask, request, redirect, url_for, jsonify
from flask_cors import CORS, cross_origin
from werkzeug.utils import secure_filename

from subprocess import call

UPLOAD_FOLDER = '/tmp'

app = Flask(__name__)
CORS(app)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/supernova', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':

        for key, file in request.files.iteritems():
            print "RECEIVED FILE with filename:", file.filename

            if file.filename == '':
                return jsonify(error="no file")

            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            print "SAVED FILE TO:", filename, "under directory:", app.config['UPLOAD_FOLDER']

        call(["python", "run_dnn.py", "test", 
            "--input_csv", "data/fake38744_4sigma_20160512-scaled2.csv",
            "--output_csv", "result-dnn.csv",
            "--model_dir=dnn_models",
            "--n_hidden_layer1", "100",
            "--n_hidden_layer2", "50",
            "--n_epochs", "100",
            "--use_gpu"])

        return jsonify(msg="Run run_dnn.py and generated result-dnn.csv",images=[True, True])

    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''

if __name__ == "__main__":
    app.run(host='0.0.0.0')
