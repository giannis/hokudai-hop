#!/bin/bash
set -e

mkdir -p "$PGDATA"
chmod 700 "$PGDATA"
chown -R postgres "$PGDATA"

chmod g+s /run/postgresql
chown -R postgres /run/postgresql

gosu postgres initdb

if [ "$POSTGRES_PASSWORD" ]; then
    pass="PASSWORD '$POSTGRES_PASSWORD'"
    authMethod=md5
else
    pass=
    authMethod=trust
fi

{ echo; echo "host all all 0.0.0.0/0 $authMethod"; } >> "$PGDATA/pg_hba.conf"

# internal start of server in order to allow set-up using psql-client		
# does not listen on external TCP/IP and waits until start finishes
gosu postgres pg_ctl -D "$PGDATA" \
    -o "-c listen_addresses='localhost'" \
    -w start

gosu postgres createuser -l demo
gosu postgres pg_restore -d postgres -Fc -C /initdb.d/testdb.bak

gosu postgres pg_ctl -D "$PGDATA" -m fast -w stop

echo
echo 'PostgreSQL init process complete'
echo
