#!/bin/bash
set -e

[ "$1" = "bash" ] && exec bash

gosu postgres pg_ctl -D "$PGDATA" \
    -o "-c listen_addresses='localhost'" \
    -w start

cd /home/demo/django/vlsvlWeb/vlsvlWeb
exec python manage.py runserver 0.0.0.0:80
