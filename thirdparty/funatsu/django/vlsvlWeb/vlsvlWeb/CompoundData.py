'''
Created on Jan 27, 2015

@author: fujihara
'''
from __builtin__ import classmethod

import sys
import os
import csv
import pygraphviz as pgv
import networkx as nx
from vlsvlWeb import util,search,settings,tfmnam_fw
from rdkit import Chem
from vlsvlWeb.util import smiles2png

class CompoundProperties():
    Id=0;
    Smiles=""
    InChiKey=""
    Url_2D=""
    MW=0
    TPSA=0.0
    LogP=0.0
    

class Synthesis():
    a_inchi_key=""
    a_smiles=""
    b_inchi_key=""
    b_smiles=""
    production_id=""
    tfm=""

class Compound():
    #properties = CompoundProperties()
    #retrosynthesises=[]
    #synthesises=[]
    #reactionGraphFile=''
    
    def __init__(self):
        self.properties = CompoundProperties()
        self.retrosynthesises =[]
        self.synthesises=[]
        self.reactionGraphFile=''
        
    def setProperties(self,smiles):
        db = util.pgConnect()
        c = db.cursor()
        sql = search.getSqlCompoundProperty()
        print sql
        c.execute(sql,(smiles,))
    
        row = c.fetchone()
        result = search.Result()
        if not row :
            print "no exist smiles: " + smiles
            result.Smiles = smiles
            self.properties.Smiles=result.Smiles
        else:
            print "id                            :",row[0]
            result.id = row[0]
            print "SMILES                        :",row[1]
            result.Smiles = row[1]
            print "AMW                           :",row[2]
            result.MW = row[2]
            print "logP                          :",row[3]
            result.LogP = row[3]
            print "Rotatable Bonds               :",row[4]
            result.RotatableBonds =row[4]
            print "toplogical polar surface area :",row[5]
            result.PolarSufaceArea=row[5]
            print "Hydrogen Donors               :",row[6]
            result.HDonors=row[6]
            print "Hydrogen Acceptors            :",row[7]
            result.HAcceptors=row[7]
            #print "inchi                         :",row[8]
            #result.InChi=row[8]
            print "inchikey                      :",row[8]
            result.InChiKey=row[8]
            
            self.properties.Url_2D=str(result.id) + ".svg"
            image_path = os.path.join(settings.STRUCT_IMG_DIR, self.properties.Url_2D)
            print "image_path " , image_path
            if not os.path.exists(image_path):
                util.smiles2SVG(smiles, image_path)
            
    
            self.properties.Id=result.id
            self.properties.Smiles=result.Smiles
            self.properties.InChiKey=result.InChiKey
            self.properties.MW=result.MW
            self.properties.LogP=result.LogP
            self.properties.TPSA=result.PolarSufaceArea
       
        c.close()
        db.close()
        
    def setPropertiesFromId(self,id):
        db = util.pgConnect()
        c = db.cursor()
        sql = search.getSqlCompoundPropertyFromId()
        print sql
        c.execute(sql,(id,))
    
        row = c.fetchone()
        result = search.Result()
        if not row:
            result.id = id
            self.properties.Id=result.id
        else:
            print "id                            :",row[0]
            result.id = row[0]
            print "SMILES                        :",row[1]
            result.Smiles = row[1]
            print "AMW                           :",row[2]
            result.MW = row[2]
            print "logP                          :",row[3]
            result.LogP = row[3]
            print "Rotatable Bonds               :",row[4]
            result.RotatableBonds =row[4]
            print "toplogical polar surface area :",row[5]
            result.PolarSufaceArea=row[5]
            print "Hydrogen Donors               :",row[6]
            result.HDonors=row[6]
            print "Hydrogen Acceptors            :",row[7]
            result.HAcceptors=row[7]
            #print "inchi                         :",row[8]
            #result.InChi=row[8]
            print "inchikey                      :",row[8]
            result.InChiKey=row[8]
            
            self.properties.Url_2D=str(result.id) + ".svg"
            image_path = os.path.join(settings.STRUCT_IMG_DIR, self.properties.Url_2D)
            print "image_path " , image_path
            if not os.path.exists(image_path):
                util.smiles2SVG(result.Smiles, image_path)
            self.properties.Id=result.id
            self.properties.Smiles=result.Smiles
            self.properties.InChiKey=result.InChiKey
            self.properties.MW=result.MW
            self.properties.LogP=result.LogP
            self.properties.TPSA=result.PolarSufaceArea
               
        c.close()
        db.close()
        
    def setPropertiesFromIchikey(self,inchikey):
        db = util.pgConnect()
        c = db.cursor()
        sql = search.getSqlIdFromInchikey()
        print sql
        c.execute(sql,(inchikey,))
        row = c.fetchone()
        id=""
        if row:
            id = row[0]
        else:
            print "not exist: " + inchikey 
        c.close()
        db.close()
        
        if id:
            self.setPropertiesFromId(id)
        else:
            self.properties.InChiKey=inchikey
                    
            
    def setRetrosynthesises(self):
        #print >> sys.stderr, "setRetrosynthesises(self)"
        value = util.getEdges(settings.REDGE_FILE_DIR,self.properties.Smiles)
        #print >> sys.stderr, "util.getEdges"
        if not value :
            print >> sys.stderr, "no edges : %s"  % self.properties.Smiles
        else:
            edges = value.split(',')    
            for edge in edges:
                #print >> sys.stderr , edge
                reSynthesis = Synthesis()
                reSynthesis.b_inchi_key = self.properties.InChiKey
                reSynthesis.b_smiles = self.properties.Smiles
                
                node = edge.split(':')[0]
                productionInfo = edge.split(':')[1]    
                mol = Chem.MolFromSmiles(node)
                #print >> sys.stderr , "mol = Chem.MolFromSmiles(node)"
                inchi = Chem.MolToInchi(mol)
                #print >> sys.stderr , "inchi = Chem.MolToInchi(mol)"
                ikey = Chem.InchiToInchiKey(inchi)
                #print >> sys.stderr , "ikey = Chem.InchiToInchiKey(inchi) " + ikey
                reSynthesis.a_inchi_key=ikey
                reSynthesis.a_smiles=node
                    
                productionIds =productionInfo.split("_")
                reSynthesis.production_id = productionIds[0]
                reSynthesis.tfm=productionIds[1]
                #print >> sys.stderr , reSynthesis.production_id 
                    
                self.retrosynthesises.append(reSynthesis)
                
                #print >> sys.stderr , "end setRetrosynthesises"
            
        
        
    def setSynthesises(self):
        value = util.getEdges(settings.EDGE_FILE_DIR,self.properties.Smiles)
        if not value :
            print "no edges : %s"  % self.properties.Smiles
       
        else:
            edges = value.split(',')    
            for edge in edges:     
                synthesis = Synthesis()
                synthesis.a_inchi_key=self.properties.InChiKey
                synthesis.a_smiles=self.properties.Smiles
                smiles =edge.split(':')[0]
                productionInfo = edge.split(':')[1]
                mol = Chem.MolFromSmiles(smiles)
                inchi = Chem.MolToInchi(mol)
                ikey = Chem.InchiToInchiKey(inchi)
                synthesis.b_inchi_key = ikey
                synthesis.b_smiles=smiles
                
                productionIds = productionInfo.split("_")
                synthesis.production_id = productionIds[0]
                synthesis.tfm=productionIds[1]
                
                self.synthesises.append(synthesis)
    
    def createReactionNxGraph(self):
        
        if not self.properties.InChiKey:
            print "no inchikey" 
            return 0
         
        G= nx.MultiDiGraph()
        G.graph['graph']={'rankdir':'LR'}
        
        G.add_node(self.properties.InChiKey, color="red", shape="record", fontname='Courier' , fontsize='12')
        
        #synthesis
        for react in self.synthesises:
            tfm_name = tfmnam_fw.Tfmname.get(int(react.tfm[0:3]))    
            G.add_edge(react.a_inchi_key,react.b_inchi_key, label = tfm_name ,fontname='Courier',fontsize='12')
            G.node[react.b_inchi_key]={'shape':'record','fontname':'Courier','fontsize':'12'}
        #resynthesis
        for react in self.retrosynthesises:
            tfm_name = tfmnam_fw.Tfmname.get(int(react.tfm[0:3])) 
            G.add_edge(react.a_inchi_key,react.b_inchi_key, label = tfm_name, fontname='Courier',fontsize='12',style='dashed', dir='back')
            G.node[react.a_inchi_key]={'shape':'record','fontname':'Courier','fontsize':'12'}
        
        g=nx.to_agraph(G)
        self.reactionGraphFile = "g_" + self.properties.InChiKey + ".svg"
        filepath = os.path.join(settings.GRAPH_IMG_DIR,self.reactionGraphFile)
        g.draw(filepath,prog='dot')
        
    def createReactionGraph(self):
        
        if not self.properties.InChiKey:
            print "no inchikey" 
            return 0
        G=pgv.AGraph(strict=False, directed=True,rankdir='LR') 
        
        
        G.add_node(self.properties.InChiKey, color="red", shape="record", fontname='Courier' , fontsize='12')
        
        #synthesis
        for react in self.synthesises:
            tfm_name = tfmnam_fw.Tfmname.get(int(react.tfm[0:3]))
            G.add_node(react.b_inchi_key, shape = 'record',fontname='Courier',fontsize='12')
            G.add_node(react.a_inchi_key, shape = 'record',fontname='Courier',fontsize='12')    
            G.add_edge(react.a_inchi_key,react.b_inchi_key, label = tfm_name ,fontname='Courier',fontsize='12')
            
        #resynthesis
        for react in self.retrosynthesises:
            tfm_name = tfmnam_fw.Tfmname.get(int(react.tfm[0:3]))
            G.add_node(react.a_inchi_key, shape='record',fontname='Courier',fontsize='12')
            G.add_node(react.b_inchi_key, shape='record',fontname='Courier',fontsize='12') 
            G.add_edge(react.a_inchi_key,react.b_inchi_key, label = tfm_name, fontname='Courier',fontsize='12',style='dashed', dir='back')
            
        
        
        self.reactionGraphFile = "g_" + self.properties.InChiKey + ".svg"
        filepath = os.path.join(settings.GRAPH_IMG_DIR,self.reactionGraphFile)
        G.draw(filepath,prog='dot')