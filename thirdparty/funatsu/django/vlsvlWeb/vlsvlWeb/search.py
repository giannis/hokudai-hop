'''
Created on Jan 21, 2014

@author: fujihara
'''

from vlsvlWeb import settings

const_select_bak = "select id, smiles, mol_amw(smiles), mol_logp(smiles), mol_numrotatablebonds(smiles),\
        mol_tpsa(smiles), mol_hbd(smiles), mol_hba(smiles), mol_inchi(smiles), mol_inchikey(smiles) "

const_select = "select id, str_smiles, amw, slogp, nrotbonds,\
        mol_tpsa(smiles), nlipinskid, nlipinskia,  inchikey "

      
class Result():
    '''
    classdocs
    '''
    Id=0
    Smiles=""
    MW=0
    Formula=""
    LogP=0
    RotatableBonds=0
    PolarSufaceArea=0
    HDonors=0
    HAcceptors=0
    InChi=""
    InChiKey=""
    Similarity=0
    ImgFile=""
'''  
class CompoundProperties():
    Id=0;
    Smiles=""
    InChiKey=""
    Url_2D=""
    MW=0
    TPSA=0.0
    LogP=0.0

class synthesis():
    a_inichi_key=""
    b_inichi_key=""
    production_id=""
    tfm_id=""

class Compound():
    properties = CompoundProperties()
    retrosynthesises=[]
    syntheises=[]
'''    
     
def createWhere_bak(request):
    
    smiles = request.session['SMILES']
    smilarity = request.session['Similarity']
    mw_gle = request.session['MW_gle']
    mw_lte = request.session['MW_lte']
    logP_gle = request.session['LogP_gle']
    logP_lte = request.session['LogP_lte']
    psa_gle = request.session['PSA_gle']
    psa_lte = request.session['PSA_lte']
    hbd_gle = request.session['Hbd_gle']
    hbd_lte = request.session['Hbd_lte']
    hba_gle = request.session['Hba_gle']
    hba_lte = request.session['Hba_lte']
    rb_gle = request.session['Rb_gle']
    rb_lte = request.session['Rb_lte']
    sql_where=" "
    
    if mw_gle :
        sql_where += str(mw_gle) + " <= mol_amw(smiles) " +" AND "
    if mw_lte :
        sql_where += str(mw_lte) + " >= mol_amw(smiles) " + " AND "
    if logP_gle:
        sql_where += str(logP_gle) + " <= mol_logp(smiles) " +" AND "
    if logP_lte:
        sql_where += str(logP_lte) + " >= mol_logp(smiles) " +" AND "
    if psa_gle:
        sql_where += str(psa_gle) + " <= mol_tpsa(smiles)" + " AND "
    if psa_lte:
        sql_where += str(psa_lte) + " >=  mol_tpsa(smiles)" + " AND "
    if hbd_gle:
        sql_where += str(hbd_gle) + " <=  mol_hbd(smiles)" + " AND "
    if hbd_lte:
        sql_where += str(hbd_lte) + " >=  mol_hbd(smiles)" + " AND "
    if hba_gle:
        sql_where += str(hba_gle) + " <=  mol_hba(smiles)" + " AND "
    if hba_lte:
        sql_where += str(hba_lte) + " >=  mol_hba(smiles)" + " AND "
    if rb_gle:
        sql_where += str(rb_gle) + " <=  mol_numrotatablebonds(smiles)" + " AND "
    if rb_lte:
        sql_where += str(rb_lte) + " >= mol_numrotatablebonds(smiles)" + " AND "
    
    sql_where = sql_where.strip()
    return  sql_where.rstrip('AND')

def createWhere_mol(request):
    
    smiles = request.session['SMILES']
    smilarity = request.session['Similarity']
    mw_gle = request.session['MW_gle']
    mw_lte = request.session['MW_lte']
    logP_gle = request.session['LogP_gle']
    logP_lte = request.session['LogP_lte']
    psa_gle = request.session['PSA_gle']
    psa_lte = request.session['PSA_lte']
    hbd_gle = request.session['Hbd_gle']
    hbd_lte = request.session['Hbd_lte']
    hba_gle = request.session['Hba_gle']
    hba_lte = request.session['Hba_lte']
    rb_gle = request.session['Rb_gle']
    rb_lte = request.session['Rb_lte']
    sql_where=" "
    
    if mw_gle :
        sql_where += str(mw_gle) + " <= amw " +" AND "
    if mw_lte :
        sql_where += str(mw_lte) + " >= amw " + " AND "
    if logP_gle:
        sql_where += str(logP_gle) + " <= slogp " +" AND "
    if logP_lte:
        sql_where += str(logP_lte) + " >= slogp " +" AND "
    if psa_gle:
        sql_where += str(psa_gle) + " <= mol_tpsa(smiles)" + " AND "
    if psa_lte:
        sql_where += str(psa_lte) + " >=  mol_tpsa(smiles)" + " AND "
    if hbd_gle:
        sql_where += str(hbd_gle) + " <=  nlipinskid " + " AND "
    if hbd_lte:
        sql_where += str(hbd_lte) + " >=  nlipinskid " + " AND "
    if hba_gle:
        sql_where += str(hba_gle) + " <=  nlipinskia " + " AND "
    if hba_lte:
        sql_where += str(hba_lte) + " >=  nlipinskia " + " AND "
    if rb_gle:
        sql_where += str(rb_gle) + " <=  nrotbonds " + " AND "
    if rb_lte:
        sql_where += str(rb_lte) + " >= nrotbonds " + " AND "
    
    sql_where = sql_where.strip()
    return  sql_where.rstrip('AND') 
def createWhere(request):
    
    smiles = request.session['SMILES']
    smilarity = request.session['Similarity']
    mw_gle = request.session['MW_gle']
    mw_lte = request.session['MW_lte']
    logP_gle = request.session['LogP_gle']
    logP_lte = request.session['LogP_lte']
    psa_gle = request.session['PSA_gle']
    psa_lte = request.session['PSA_lte']
    hbd_gle = request.session['Hbd_gle']
    hbd_lte = request.session['Hbd_lte']
    hba_gle = request.session['Hba_gle']
    hba_lte = request.session['Hba_lte']
    rb_gle = request.session['Rb_gle']
    rb_lte = request.session['Rb_lte']
    sql_where=" "
    
    if mw_gle :
        sql_where += str(mw_gle) + " <= amw " +" AND "
    if mw_lte :
        sql_where += str(mw_lte) + " >= amw " + " AND "
    if logP_gle:
        sql_where += str(logP_gle) + " <= slogp " +" AND "
    if logP_lte:
        sql_where += str(logP_lte) + " >= slogp " +" AND "
    if psa_gle:
        sql_where += str(psa_gle) + " <= mol_tpsa(smiles)" + " AND "
    if psa_lte:
        sql_where += str(psa_lte) + " >=  mol_tpsa(smiles)" + " AND "
    if hbd_gle:
        sql_where += str(hbd_gle) + " <=  nlipinskid " + " AND "
    if hbd_lte:
        sql_where += str(hbd_lte) + " >=  nlipinskid " + " AND "
    if hba_gle:
        sql_where += str(hba_gle) + " <=  nlipinskia " + " AND "
    if hba_lte:
        sql_where += str(hba_lte) + " >=  nlipinskia " + " AND "
    if rb_gle:
        sql_where += str(rb_gle) + " <=  nrotbonds " + " AND "
    if rb_lte:
        sql_where += str(rb_lte) + " >= nrotbonds " + " AND "
    
    sql_where = sql_where.strip()
    return  sql_where.rstrip('AND')    
def getSql(request):
    str_select = const_select
    sql = str_select + " from " + settings.VLSVL_TABLE + "  Where " 
    #sql += sql_where.rstrip('AND')
    sql += createWhere(request)
    sql += " Limit " + settings.VLSVL_LIMIT +" OFFSET 0"
    return sql

def getSqlSimilarity(request):
    """ structure similarity search  %s,%s,%s = smiles,smiles,similarliry
    
    """
    str_select = const_select + ", tanimoto_sml(atompairbv_fp(smiles),atompairbv_fp(%s)) AS TANIMOTO_SIM "
    str_where ="" 
    str_where += createWhere(request) 
    
    smilarity = request.session['Similarity']
    print smilarity
    
    if str_where :
        str_where += " AND " 
    if int(smilarity) < 100:
        str_where += "tanimoto_sml(atompairbv_fp(smiles),atompairbv_fp(%s)) >= %s "
    else:
        str_where += "str_smiles = " + request.session['SMILES']
        
    str_where += " Order by TANIMOTO_SIM  DESC " 
    sql = str_select +  "From " + settings.VLSVL_TABLE + "  Where "  + str_where
    sql += " Limit " + settings.VLSVL_LIMIT + " OFFSET 0"
    return sql

def getSqlSimilarity_100(request):
    """ structure similarity search  %s,%s,%s = smiles,smiles,similarliry
    
    """
    str_select = const_select 
    str_where ="" 
    str_where += createWhere(request) 
    
    
    if str_where :
        str_where += " AND "
         
    str_where += " str_smiles =%s "
        
    sql = str_select +  "From " + settings.VLSVL_TABLE + "  Where "  + str_where
    sql += " Limit " + settings.VLSVL_LIMIT + " OFFSET 0"
    return sql

def getSqlSubstructure(request):
    """ substructure search  %s = query of smiles
    
    """
    str_select =""
    str_select = const_select
    str_where =""
    str_where += createWhere(request)
    if str_where :
        str_where += " AND "
    str_where +=" smiles@>%s "
    sql = str_select + " From " + settings.VLSVL_TABLE + " Where " + str_where
    sql += " Limit " + settings.VLSVL_LIMIT + " OFFSET 0"
    return sql
def getSqlCompoundProperty():
    
    str_select = const_select
    str_where = " Where str_smiles = %s ;"
    sql = str_select +" From " + settings.VLSVL_TABLE + str_where
    return sql

def getSqlCompoundPropertyFromId():
    
    str_select = const_select
    str_where = " Where id = %s ;"
    sql = str_select +" From " + settings.VLSVL_TABLE + str_where
    return sql
     
def getSqlIdFromInchikey():
    
    str_select = " Select id "
    str_where = " Where inchikey= %s ;"
    sql = str_select +" From " + settings.VLSVL_TABLE + str_where
    return sql

