'''
Created on Jan 21, 2014

@author: fujihara
'''

from django import forms

class EntryForm(forms.Form):
    '''
    classdocs
    '''

    SMILES = forms.CharField(
        label ='SMILES',required = False
    )
    SimilarityOPTs=(("0","substructure"),("100","100"),("90","90"),("80","80"),("70","70"),("60","60"),("50","50"),)
    Similarity = forms.ChoiceField(choices=SimilarityOPTs,initial=0,)
    #Similarity = forms.FloatField(
    #    label ='similarity' 
    #)
    MW_gle = forms.FloatField(
        label = 'mw_gle', required = False 
    )
    MW_lte = forms.FloatField(
        label = 'mw_lte', required = False  , min_value=0             
    )
    LogP_gle=forms.FloatField(
        label = 'lopP_gle', required = False
    )
    LogP_lte=forms.FloatField(
        label = 'logP_lte', required = False
    )
    PSA_gle=forms.FloatField(
         label = 'PSA_gle' , required = False                                            
    )
    PSA_lte=forms.FloatField(
         label = 'PSA_lte',required = False                                             
    )
    Hbd_gle = forms.IntegerField(
        label = 'hbd_gle',required = False
    )
    Hbd_lte = forms.IntegerField(
        label = 'hbd_lte',required = False
    )
    Hba_gle = forms.IntegerField(
        label = 'hba_gle',required = False
    )
    Hba_lte = forms.IntegerField(
        label = 'hba_lte',required = False
    )
    Rotatable_bonds_gle = forms.IntegerField(
        label = 'rb_gle',required = False
    )
    Rotatable_bonds_lte = forms.IntegerField(
        label = 'rb_lte',required = False
    )
