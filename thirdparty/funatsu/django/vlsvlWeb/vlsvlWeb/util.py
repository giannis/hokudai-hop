'''
Created on Feb 14, 2014

@author: fujihara
'''
import sys
import os
import psycopg2
import glob
from rdkit import Chem
from rdkit.Chem import Draw
from rdkit.Chem import AllChem
from rdkit.Chem import rdmolfiles
from django.conf import settings
from kyotocabinet import *

def smiles2png(smiles,output,x,y):
    mol = AllChem.MolFromSmiles(smiles)
    AllChem.Compute2DCoords(mol)
    Draw.MolToFile(mol, output,size=(x,y))

def smiles2pngS(smiles,filePath):
    smiles2png(smiles,filePath,100,100)

def smiles2SVG(smiles, filePath):
    mol = AllChem.MolFromSmiles(smiles)
    AllChem.Compute2DCoords(mol)
    Draw.MolToFile(mol, filePath,imageType='svg')
    
def smiles2SVGs(smiles, filePath):
    mol = AllChem.MolFromSmiles(smiles)
    AllChem.Compute2DCoords(mol)
    Draw.MolToFile(mol, filePath,imageType='svg',size=(100,100))    

def imgFilePath(dir,name):
    return dir + os.sep + name

def smallImgFile(id):
    imgFile = str(id) + "_s.svg"
    return imgFile

def pgConnect():
    db = psycopg2.connect(host=settings.PG_HOST, dbname=settings.PG_DBNAME, user=settings.PG_USER, password=settings.PG_PASSWD )
    return db

def edgeFilePath(dir,inchikey):
    first =inchikey[0:2]
    second = inchikey[2:4]
    return os.path.join(dir,first,second,inchikey)

def writeSDF(dir, id):
    #db = psycopg2.connect(host="vlsvl", dbname="testdb", user="fujihara", password="" )
    db=pgConnect();
    c = db.cursor()
    c = db.cursor()
    sql = "select id, smiles from vlsvl where id = " + id + " ;"
    c.execute(sql);
    id =0
    smiles=""
    for row in c:
        print "id       ", row[0]
        print "SMILES   ", row[1]
        id = row[0]
        smiles = row[1]
    c.close
    db.close
    
    mol = Chem.MolFromSmiles(smiles)
    sdfFile = "id_" + str(id) + ".sdf"
    sdfpath = os.path.join(dir,sdfFile)
    if os.path.exists(sdfpath) == False:
        writer = Chem.rdmolfiles.SDWriter(sdfpath)
        writer.write(mol)
    
def getEdges(dir,smiles):
    kvEdges = glob.glob(os.path.join(dir,"*.kch"))
    db = DB()
    value =""
    for kchfile in kvEdges:
        path = os.path.join(dir,kchfile)
        if not db.open(path,DB.OREADER):
            print "KC DB open error:" + str(db.error()) + " " + path
            continue
        
        value = db.get(smiles)
        if value:
            break
        if  not db.close():
            print "KC DB close error: " + str(db.error())
    return value


    
    