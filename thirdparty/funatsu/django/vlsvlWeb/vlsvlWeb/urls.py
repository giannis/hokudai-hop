from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'vlsvlWeb.views.home', name='home'),
    # url(r'^vlsvlWeb/', include('vlsvlWeb.foo.urls')),
     url(r'^$', 'vlsvlWeb.views.index'),
    # url(r'^vlsvlWeb/search/$', 'vlsvlWeb.views.search'),
     url(r'^search/$', 'vlsvlWeb.views.search'),
     #url(r'^vlsvlWeb/results/$', 'vlsvlWeb.views.results'),
     url(r'^results/$', 'vlsvlWeb.views.results'),
     #url(r'^vlsvlWeb/results_similarity/$', 'vlsvlWeb.views.results_similarity'),
     url(r'^results_similarity/$', 'vlsvlWeb.views.results_similarity'),
     #url(r'^vlsvlWeb/results_substructure/$', 'vlsvlWeb.views.results_substructure'),
     url(r'^results_substructure/$', 'vlsvlWeb.views.results_substructure'),
     #url(r'^vlsvlWeb/viewAll/$', 'vlsvlWeb.views.viewAll'),
     url(r'^viewAll/$', 'vlsvlWeb.views.viewAll'),
     url(r'^createSDF/$', 'vlsvlWeb.views.createSDF'),
     url(r'^compound/$', 'vlsvlWeb.views.compound'),
     url(r'^compound_id/$', 'vlsvlWeb.views.compound_id'),
     url(r'^compound_inchikey/$', 'vlsvlWeb.views.compound_inchikey'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
