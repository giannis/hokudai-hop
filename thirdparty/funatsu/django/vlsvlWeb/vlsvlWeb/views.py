'''
Created on Jan 21, 2014

@author: fujihara
'''
from django.shortcuts import render, render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from django.template import loader, RequestContext
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_protect
from django.core.context_processors import csrf

import sys
import psycopg2
import os.path

from rdkit import Chem
#import Chem
from rdkit.Chem  import rdMolDescriptors
#from Chem  import rdMolDescriptors

from django.conf import  settings
from vlsvlWeb import CompoundData
from vlsvlWeb.util import smiles2png, smiles2pngS, smallImgFile, imgFilePath, writeSDF, pgConnect
from vlsvlWeb.form import EntryForm
from vlsvlWeb.search import Result, getSql, getSqlSimilarity, getSqlSubstructure,\
    getSqlSimilarity_100

#STRUCT_IMG_DIR=settings.STRUCT_IMG_DIR
SDF_DIR=settings.SDF_DIR
#PG_HOST="vlsvl"

def index(request):
    contexts = RequestContext(request, {'message': ' Hello Django',})
    template = loader.get_template('index.html')
    #return HttpResponse('Hello Django')
    return HttpResponse(template.render(contexts))

def search(request):
    if request.method == 'POST':
        form = EntryForm(request.POST)
        if form.is_valid():
            # search DB
            request.session['SMILES']=form.cleaned_data['SMILES']
            request.session['Similarity']=form.cleaned_data['Similarity']
            request.session['MW_gle']=form.cleaned_data['MW_gle']
            request.session['MW_lte']=form.cleaned_data['MW_lte']
            request.session["LogP_gle"]=form.cleaned_data['LogP_gle']
            request.session["LogP_lte"]=form.cleaned_data['LogP_lte']
            request.session["PSA_gle"]=form.cleaned_data['PSA_gle']
            request.session["PSA_lte"]=form.cleaned_data['PSA_lte']
            request.session["Hbd_gle"]=form.cleaned_data['Hbd_gle']
            request.session["Hbd_lte"]=form.cleaned_data['Hbd_lte']
            request.session["Hba_gle"]=form.cleaned_data['Hba_gle']
            request.session["Hba_lte"]=form.cleaned_data['Hba_lte']
            request.session["Rb_gle"]=form.cleaned_data['Rotatable_bonds_gle']
            request.session["Rb_lte"]=form.cleaned_data['Rotatable_bonds_lte']
            #print("dump")
            #print('SMILES=%s Similarity=%s MW_gle=%s MW_lte=%s' \
            #       %(request.session['SMILES'],request.session['Similarity'],request.session['MW_gle'],request.session['MW_lte']))
            # structure search
            print request.session['Similarity']
            if request.session['SMILES'] and float(request.session['Similarity']) == 0 :
                return HttpResponseRedirect(reverse('vlsvlWeb.views.results_substructure')) 
            elif request.session['SMILES'] :
                return HttpResponseRedirect(reverse('vlsvlWeb.views.results_similarity'))
               
            else :
                return HttpResponseRedirect(reverse('vlsvlWeb.views.results'))
            
    else:
        form = EntryForm()
    #return render_to_response('search.html',{'form':form})
    return render_to_response('search.html',context_instance=RequestContext(request,{'forms':form}))


def results(request):
    #global IMG_DIR
    #global PG_HOST
    #return HttpResponseRedirect('http://www.yahoo.co.jp/') 
    
    #if request.method == 'GET':
    print('SMILES=%s Similarity=%s MW_gle=%s MW_lte=%s' \
                   %(request.session['SMILES'],request.session['Similarity'],request.session['MW_gle'],request.session['MW_lte']))
    smiles = request.session['SMILES']
    smilarity = request.session['Similarity']
    mw_gle = request.session['MW_gle']
    mw_lte = request.session['MW_lte']
    
    
    #db = psycopg2.connect( host=PG_HOST, dbname="testdb", user="fujihara", password="" )
    db = pgConnect()
    c = db.cursor()
    # test sql
    sql = "select id, smiles, mol_amw(smiles), mol_logp(smiles), mol_numrotatablebonds(smiles),\
        mol_tpsa(smiles), mol_hbd(smiles), mol_hba(smiles), mol_inchi(smiles),\
        mol_inchikey(smiles)  from vlsvl Where %s < mol_amw(smiles) AND mol_amw(smiles) < %s limit 1000;"
    #test sql
    
    sql = getSql(request)
    print sql
    c.execute(sql,)
    #c.execute(sql,(mw_gle, mw_lte));

    results =[]
    #print "###############################"
    for row in c:
    #    print "id                            :",row[0]
        result = Result()
        
        result.Id = row[0]
    #    print "SMILES                        :",row[1]
        result.Smiles = row[1]
    #   print "AMW                           :",row[2]
        result.MW = row[2]
    #   print "logP                          :",row[3]
        result.LogP = row[3]
    #   print "Rotatable Bonds               :",row[4]
        result.RotatableBonds =row[4]
    #   print "toplogical polar surface area :",row[5]
        result.PolarSufaceArea=row[5]
    #   print "Hydrogen Donors               :",row[6]
        result.HDonors=row[6]
    #   print "Hydrogen Acceptors            :",row[7]
        result.HAcceptors=row[7]
    #    print "inchi                         :",row[8]
    #    result.InChi=row[8]
    #    print "inchikey                      :",row[9]
        result.InChiKey=row[8]
    # create ImageFile    
        result.ImgFile = smallImgFile(result.Id)
        if os.path.exists(imgFilePath(settings.STRUCT_IMG_DIR, result.ImgFile)) == False:
            smiles2pngS(result.Smiles,imgFilePath(settings.STRUCT_IMG_DIR, result.ImgFile))
    #   formula
        result.Formula = Chem.rdMolDescriptors.CalcMolFormula(Chem.MolFromSmiles(result.Smiles))
    
        results.append(result)
        
    c.close;
    db.close;
    #results.append(result)
    return render_to_response('results.html',context_instance=RequestContext(request,{'results':results}))
        #contexts = RequestContext(request,{'results':results,})
        #template = loader.get_template('results.html')
        #return HttpResponseRedirect('http://www.yahoo.com/')
        #return HttpResponse(template.render(contexts))
    #else :
    #   return HttpResponseRedirect('http://www.yahoo.co.jp/')

def results_similarity(request):
    global PG_HOST
    #return HttpResponseRedirect('http://www.yahoo.co.jp/') 
    
    smiles = request.session['SMILES']
    smilarity = request.session['Similarity']
    
    #db = psycopg2.connect(host=settings.PG_HOST, dbname="testdb", user="fujihara", password="")
    db = pgConnect()
    c = db.cursor()
    # test sql
    sql = "select id, smiles, mol_amw(smiles), mol_logp(smiles), mol_numrotatablebonds(smiles),\
        mol_tpsa(smiles), mol_hbd(smiles), mol_hba(smiles), mol_inchi(smiles),\
        mol_inchikey(smiles)  from vlsvl Where %s < mol_amw(smiles) AND mol_amw(smiles) < %s Limit 1000;"
    #test sql
    
    float_sim=float(smilarity)
    if float_sim <100 :
        sql = getSqlSimilarity(request)
        c.execute(sql,(smiles,smiles,float_sim/100,))
    else:
        sql =getSqlSimilarity_100(request)
        print sql
        c.execute(sql,(smiles,))
    
    
    print smiles
    print float_sim
    
    
    #c.execute(sql,(mw_gle, mw_lte));
    results =[]
    #print "###############################"
    for row in c:
    #    print "id                            :",row[0]
        result = Result()
        
        result.Id = row[0]
    #    print "SMILES                        :",row[1]
        result.Smiles = row[1]
    #   print "AMW                           :",row[2]
        result.MW = row[2]
    #   print "logP                          :",row[3]
        result.LogP = row[3]
    #   print "Rotatable Bonds               :",row[4]
        result.RotatableBonds =row[4]
    #   print "toplogical polar surface area :",row[5]
        result.PolarSufaceArea=row[5]
    #   print "Hydrogen Donors               :",row[6]
        result.HDonors=row[6]
    #   print "Hydrogen Acceptors            :",row[7]
        result.HAcceptors=row[7]
    #    print "inchi                         :",row[8]
    #    result.InChi=row[8]
    #    print "inchikey                      :",row[9]
        result.InChiKey=row[8]
        if float_sim <100 :
            result.Similarity=round(float(row[9])*100,2)
        else:
            result.Similarity=100
    #    create ImageFile
        result.ImgFile = smallImgFile(result.Id)
        if os.path.exists(imgFilePath(settings.STRUCT_IMG_DIR, result.ImgFile)) == False:
            smiles2pngS(result.Smiles,imgFilePath(settings.STRUCT_IMG_DIR, result.ImgFile))
        
        result.Formula = Chem.rdMolDescriptors.CalcMolFormula(Chem.MolFromSmiles(result.Smiles))
        
        results.append(result)
    c.close;
    db.close;
 
    return render_to_response('results_similarity.html',context_instance=RequestContext(request,{'results':results}))
    

def results_substructure(request):
    global PG_HOST
    smiles = request.session['SMILES']
    smilarity = request.session['Similarity']
    
    #db = psycopg2.connect(host=PG_HOST, dbname="testdb", user="fujihara", password="")
    db = pgConnect()
    c = db.cursor()
    # test sql
    sql = "select id, smiles, mol_amw(smiles), mol_logp(smiles), mol_numrotatablebonds(smiles),\
        mol_tpsa(smiles), mol_hbd(smiles), mol_hba(smiles), mol_inchi(smiles),\
        mol_inchikey(smiles)  from vlsvl Where %s < mol_amw(smiles) AND mol_amw(smiles) < %s Limit 1000;"
    #test sql
    
    sql = getSqlSubstructure(request)
    print sql
    c.execute(sql,(smiles,))
    #c.execute(sql,(mw_gle, mw_lte));

    results =[]
    #print "###############################"
    for row in c:
    #    print "id                            :",row[0]
        result = Result()
        
        result.Id = row[0]
    #    print "SMILES                        :",row[1]
        result.Smiles = row[1]
    #   print "AMW                           :",row[2]
        result.MW = row[2]
    #   print "logP                          :",row[3]
        result.LogP = row[3]
    #   print "Rotatable Bonds               :",row[4]
        result.RotatableBonds =row[4]
    #   print "toplogical polar surface area :",row[5]
        result.PolarSufaceArea=row[5]
    #   print "Hydrogen Donors               :",row[6]
        result.HDonors=row[6]
    #   print "Hydrogen Acceptors            :",row[7]
        result.HAcceptors=row[7]
    #    print "inchi                         :",row[8]
    #    result.InChi=row[8]
    #    print "inchikey                      :",row[9]
        result.InChiKey=row[8]
    
    #    create ImageFile
        result.ImgFile = smallImgFile(result.Id)
        if os.path.exists(imgFilePath(settings.STRUCT_IMG_DIR, result.ImgFile)) == False:
            smiles2pngS(result.Smiles,imgFilePath(settings.STRUCT_IMG_DIR, result.ImgFile))
    #   formula
        result.Formula = Chem.rdMolDescriptors.CalcMolFormula(Chem.MolFromSmiles(result.Smiles))
        
        results.append(result)
    c.close;
    db.close;
 
    return render_to_response('results.html',context_instance=RequestContext(request,{'results':results}))
        
def viewAll(request):
    global PG_HOST
    db = psycopg2.connect(host=PG_HOST, dbname="testdb", user="fujihara", password="")
    c = db.cursor()
    sql = "select id, smiles, mol_amw(smiles), mol_logp(smiles), mol_numrotatablebonds(smiles),\
        mol_tpsa(smiles), mol_hbd(smiles), mol_hba(smiles), mol_inchi(smiles),\
        mol_inchikey(smiles)  from vlsvl Where %s < mol_amw(smiles) AND mol_amw(smiles) < %s;"
    c.execute(sql,(0,1000));

    results =[]
    print "###############################"
    for row in c:
        result = Result()
        print "id                            :",row[0]
        result.id = row[0]
        print "SMILES                        :",row[1]
        result.Smiles = row[1]
        print "AMW                           :",row[2]
        result.MW = row[2]
        print "logP                          :",row[3]
        result.LogP = row[3]
        print "Rotatable Bonds               :",row[4]
        result.RotatableBonds =row[4]
        print "toplogical polar surface area :",row[5]
        result.PolarSufaceArea=row[5]
        print "Hydrogen Donors               :",row[6]
        result.HDonors=row[6]
        print "Hydrogen Acceptors            :",row[7]
        result.HAcceptors=row[7]
        print "inchi                         :",row[8]
        result.InChi=row[8]
        print "inchikey                      :",row[9]
        result.InChiKey=row[9]
        results.append(result)
    c.close;
    db.close;
        
    return render_to_response('results.html',context_instance=RequestContext(request,{'results':results}))

def createSDF(request):
    global SDF_DIR
    
    Id = request.GET["id"]
    writeSDF(settings.SDF_DIR, Id)
    sdffile = "id_" + str(Id) + ".sdf"
    sdfpath = os.path.join(settings.SDF_DIR,sdffile)
    
    response = HttpResponse(open(sdfpath,'rb').read(), mimetype='text/download')
    name = "filename=" + sdffile
    response['Content-Disposition']= name
    return response

@csrf_protect    
def compound(request):
    smiles =""
    if request.method == "GET":
        smiles = request.GET["smiles"]
    elif request.method == "POST":
        smiles = request.POST["smiles"]
    
    compound = CompoundData.Compound()
    compound.setProperties(smiles)
    compound.setRetrosynthesises()
    compound.setSynthesises()
    compound.createReactionGraph()
    
    return render_to_response('compound.html',context_instance=RequestContext(request,{'compound':compound}))

def compound_id(request):
    
    #print >> sys.stderr, "OK request"
    id=0
    if request.method == "GET":
        id = request.GET["id"]
    elif request.method == "POST" :
        id = request.POST["id"]
    
    compound = CompoundData.Compound()
    #print >> sys.stderr, "OK CompoundData.Compound()"
    compound.setPropertiesFromId(id)
    #print >> sys.stderr,"OK compound.setPropertiesFromId(id)"
    compound.setRetrosynthesises()
    #print >> sys.stderr, "OK compound.setRetrosynthesises()"
    compound.setSynthesises()
    #print >> sys.stderr, "OK compound.setSynthesises() "
    compound.createReactionGraph()
    #print >> sys.stderr, "OK compound.createReactionGraph()"
    
    return render_to_response('compound.html',context_instance=RequestContext(request,{'compound':compound}))

def compound_inchikey(request):
    
    inchikey=""
    if request.method == "GET" :
        inchikey = request.GET["inchikey"]
    elif request.method == "POST" :
        inchikey = request.POST["inchikey"]
    
    compound = CompoundData.Compound()
    compound.setPropertiesFromIchikey(inchikey)
    compound.setRetrosynthesises()
    compound.setSynthesises()
    compound.createReactionGraph()
    
    return render_to_response('compound.html',context_instance=RequestContext(request,{'compound':compound}))
    
    

